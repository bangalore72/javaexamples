package InvokeJavaScript;


import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;



public class InvokeJavaScript {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws ScriptException{
		  ScriptEngineManager manager = new ScriptEngineManager();
	      ScriptEngine engine = manager.getEngineByName("javascript");
	 
	      engine.eval("var x = 100;");
	      engine.eval("var y = 20;");
	      engine.eval("var z = x / y;");	     
	      engine.eval("print (z);");

	}

}
