package com.telnet.core.samplePrograms;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.DateFormat;
import java.util.Date;

public class Find extends SimpleFileVisitor<Path> {
	
	public static String str = "";
 
     /** Main program.
      * @param args Command line arguments - directories to search.
      */
     public static void main(final String... args) throws IOException {
         final FileVisitor<Path> fileVisitor = new Find();
         //for (final String arg : args.length > 0 ? args : new String[] {"."}) {
            // final Path root = Paths.get(arg);
         Path dir = Paths.get("C:/JAXB-Jars");
        
             Files.walkFileTree(dir, fileVisitor);
         //}
             
             System.out.println("str >>> "+ str);
             
     }
 
     /** {@inheritDoc} */
     public FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attrs) {
         if (".svn".equals(dir.getFileName().toString())) {
             return FileVisitResult.SKIP_SUBTREE;
         }
         System.out.println("dir "+dir);
         return FileVisitResult.CONTINUE;
     }
 
     /** {@inheritDoc} */
     public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) {
    	 String creationTime =  getTime( attrs.creationTime().toMillis());
    	 String currFile = creationTime +"     "+"     " + attrs.size() + "    "+file.getFileName();
    	 str +=  currFile +"\n";
         System.out.println("visitFile "+file);
         return FileVisitResult.CONTINUE;
     }
     
     
     public static String getTime(long yourmilliseconds){
 		
 		Date date = new Date(yourmilliseconds);
 		return   DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(date).toString();
     }
 
 }
