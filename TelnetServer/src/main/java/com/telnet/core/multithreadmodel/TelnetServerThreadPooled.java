package com.telnet.core.multithreadmodel;




import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.management.RuntimeErrorException;

import com.telnet.core.util.TelnetUtil;

public class TelnetServerThreadPooled implements Runnable{
           protected int serverPort = 8080;
           protected ServerSocket serverSocket = null;
           protected boolean isStopped = false;
           protected Thread runningThread = null;
           protected ExecutorService threadPool = Executors.newFixedThreadPool(10);     //fixed thread pool
           
           
           public  TelnetServerThreadPooled(int serverPort) {
        	   this.serverPort = serverPort;
           }
           
           
           public void run() {
        	   synchronized(this) {
        		   this.runningThread = Thread.currentThread();        		   
        	   }
        	   
        	   openServerSocket();
        	   
        	   while(!isStopped) {
        		   Socket clientSocket = null;
        		   try{
        			   clientSocket = this.serverSocket.accept();
                       this.threadPool.execute(new WorkerThread(clientSocket.getInputStream(),clientSocket.getOutputStream(), "Thread pool server for each client"));
        		   }catch(IOException ex) {
        			   throw new RuntimeException("error while acception connection.");
        		   }
        	   }
        	   
        	   System.out.println("Just before shutting down.....");
        	   this.threadPool.shutdown();
        	   stop();
        	   System.out.println("Server Stopped.");
        		   
           }
           
           private void stop() {
        	   this.isStopped = true;
        	   try {
        		   this.serverSocket.close();
        	   }catch(IOException ex) {
        		   throw new RuntimeException("Error while closing server socket.");
        	   }
           }
           
           private void openServerSocket()
           {
        	   try{
        		   this.serverSocket = new ServerSocket(this.serverPort);
        	   }catch(IOException ex) {
        		  throw new RuntimeException("can't open connection on port "+ serverPort);  
        	   }
           }
           
           public static void main(String args[]) {
        	   TelnetServerThreadPooled tps = new TelnetServerThreadPooled(9000);
        	   
        	   new Thread(tps).start();
        	   
        	   System.out.println("main last line");
           }
           
           
}