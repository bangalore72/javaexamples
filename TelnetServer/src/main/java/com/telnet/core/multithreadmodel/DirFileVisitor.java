package com.telnet.core.multithreadmodel;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.DateFormat;
import java.util.Date;
import java.util.logging.Logger;



public class DirFileVisitor extends SimpleFileVisitor<Path> {
	
	private final static Logger LOGGER = Logger.getLogger(DirFileVisitor.class .getName()); 
	
	public static String str = "";
	public StringBuffer sbf = new StringBuffer();
	private int depth = -1;
 
     /** Main program.
      * @path Command line arguments - directories to search.
      */
	public static String listAllFiles(String path) throws IOException {
		str = "";
		final FileVisitor<Path> fileVisitor = new DirFileVisitor();
		//int depth = 0;

		//Path dir = Paths.get("C:/JAXB-Jars");
		Path dir = Paths.get(path);
		
		try{

		Files.walkFileTree(dir, fileVisitor);
		}catch(Exception ex) {}
		
		
		System.out.println("str >>> " + str);
		
		return str;

	}
 
     /** {@inheritDoc} */
     public FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attrs) {
    
     if(depth != -1)  {
		try {
			String creationTime = getTime(attrs.creationTime().toMillis());
			String currFile = creationTime + "            " + "<DIR>" + "     "
					+ attrs.size() + "       " + dir.getFileName();
			str += currFile + System.getProperty("line.separator");
		} catch (Exception ace) {
		}
     }	

		System.out.println("depth "+ depth + "dir name "+ dir.getFileName());
		depth++;
		return FileVisitResult.CONTINUE; 
       
     }
     
     
     
     
 
     @Override
	public FileVisitResult postVisitDirectory(Path dir, IOException e)
			throws IOException {
    	
    	 if (e == null) {
             depth--;
             System.out.println("postVisitDirectory(" + dir + ")");
             return FileVisitResult.CONTINUE;
         } else {
             throw e;
         }
    	 
        // return FileVisitResult.CONTINUE;
	}

	/** {@inheritDoc} */
     public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) {
    	 
		if (depth > 0) {
			return FileVisitResult.SKIP_SIBLINGS;
		}

		try {
			String creationTime = getTime(attrs.creationTime().toMillis());
			String currFile = creationTime + "     " + "     " + attrs.size()
					+ "       " + file.getFileName();
			str += currFile + System.getProperty("line.separator");
			System.out.println(currFile);
		} catch (Exception ace) {
		}

		return FileVisitResult.CONTINUE;
    	/* try{
    	 String creationTime =  getTime( attrs.creationTime().toMillis());
    	 String currFile = creationTime +"     "+"     " + attrs.size() + "       "+file.getFileName();
    	 str +=  currFile + System.getProperty("line.separator");
         System.out.println(currFile);
    	 }catch(Exception ace){}
         return FileVisitResult.CONTINUE;*/
     }
     
     @Override
     public FileVisitResult visitFileFailed(Path file,
                                        IOException exc) {
         System.err.println(exc);
         return FileVisitResult.CONTINUE;
     }
     
     
    /* public FileVisitResult postVisitDirectory(final Path dir, final BasicFileAttributes attrs) {
    	 System.out.println("postVisitDirecotyr "+ dir.getFileName());
    	 return FileVisitResult.CONTINUE;
     }*/
     
     
     public static String getTime(long yourmilliseconds){
 		
 		Date date = new Date(yourmilliseconds);
 		return   DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(date).toString();
     }
 

}
