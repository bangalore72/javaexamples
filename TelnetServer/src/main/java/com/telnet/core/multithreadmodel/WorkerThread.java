package com.telnet.core.multithreadmodel;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.List;
import java.util.ArrayList;

import com.telnet.core.interfaces.OSType;
import com.telnet.core.util.CLICommands;
import com.telnet.core.util.FindOS;
import com.telnet.core.util.TelnetUtil;


public class WorkerThread implements Runnable {
	
	protected Socket clientSocket;
	protected String text;
	protected String CURRENT_DIRECTORY = TelnetUtil.getUserHomeDirectory();  //initially current directory is user home directory
    private InputStream is;
    private OutputStream os;

    public void setInputStream(InputStream is) {
        this.is = is;
    }
	
	public WorkerThread(Socket clientSocket, String text) {
		this.clientSocket = clientSocket;
		this.text = text;		
	}

    public WorkerThread(InputStream is, OutputStream os, String text) {
        this.is = is;
        this.os = os;
        this.text = text;
    }
	
	public void run()  {
		try {
		  Scanner in = new Scanner(is);
		  PrintWriter out = new PrintWriter(os, true);		  
		  
		    boolean done = false;
			while (!done && in.hasNextLine()) {
				
				// read data from Socket
				String line = in.nextLine();

				if (line.trim().equalsIgnoreCase("exit")) {
					done = true;
				} else {
					//out.println("[bash~]$ " + line);
				    parseInputCommand(line, out);
				}				
			}
			is.close();
			os.close();			
		}catch(IOException ex) {
			ex.printStackTrace();
		}
	}


    /**
     *  mostly used for JUNIT testing. We need output to assert so instead of directing to a console directing to a stream
     * @param cmd
     * @param ps
     */
	public void parseInputCommand(String cmd, PrintStream ps) {
        String outputString = parseInputCommand(cmd);
        ps.append(outputString);

    }

    /** Method used to parse CLI Command  and return the output. Commands listed in switch case are supported. This can be extended for more support.
     *
     * @param cmd
     * @return
     */
    public String parseInputCommand(String cmd) {
       OSType ostype = FindOS.findOSUnderneath();   //using factory pattern concrete object will be returned based on OS Type

		StringTokenizer stk = new StringTokenizer(cmd, " ");

		cmd = stk.nextToken();

		CLICommands.OSCommands wcmd = CLICommands.OSCommands
				.value(cmd);

		if (wcmd != null) {
			
			if(FindOS.isUnix()) {
				return parseUnixCmdSwitch(wcmd, stk, ostype);
			}else if(FindOS.isWindows()) {
				return parseWindowsCmdSwitch(wcmd, stk, ostype);
			}else {
				return TelnetUtil.YET_TO_IMPLEMENT;
			}
		
		}else {
			return TelnetUtil.UNABLE_TO_FIND_COMMAND_ERROR;
		}
    }    


    /** Overloaded method for printing on the console. 
     *
     * @param cmd
     * @param out
     */
	private void parseInputCommand(String cmd, PrintWriter out) {

         String outputString = parseInputCommand(cmd);
		 //System.out.println(outputString);
         out.println(outputString);		
	}
	
	/** Methdo to parse unix commands. Main method for unix commands
	 * 
	 * @param wcmd
	 * @param stk
	 * @param ostype
	 * @return
	 */
	private String parseUnixCmdSwitch(CLICommands.OSCommands wcmd, StringTokenizer stk, OSType ostype) {
		
		switch (wcmd) {

        case LS:
        case DIR:	
            String lsParams = null;
			try{
				lsParams = stk.nextToken();
			}catch(Exception ex){lsParams = null;}
            String list = ostype.listCurrentDirecotryDetails(lsParams, CURRENT_DIRECTORY);
            return list;        
		case PWD:				
			return CURRENT_DIRECTORY;
        case RM:
            List<String> listAfterRM = TelnetUtil.parseStringTokenFurther(stk);
            if(listAfterRM != null && listAfterRM.size() >= 2)  {
                 String params = listAfterRM.get(0);
                 if(params.indexOf("r") >=0 || params.indexOf("f") >=0 || params.indexOf("*") >=0)  {
                   return  ostype.removeFileOrDirectory(listAfterRM.get(1), true, CURRENT_DIRECTORY);
                 }else{
                   return TelnetUtil.COMMAND_NOT_SUPPORTED;  
                 }
            }else if(listAfterRM != null && listAfterRM.size() ==1) {
                return  ostype.removeFileOrDirectory(listAfterRM.get(0), false, CURRENT_DIRECTORY);
            }else{
                return TelnetUtil.getRMErrorBasedOnOS(null);
            }
            
        case CD:
			String stringAfterCD = null;
			try{
				stringAfterCD = stk.nextToken();
			}catch(Exception ex){stringAfterCD = null;}

			String finalPath = ostype.executeCDCommand(stringAfterCD, CURRENT_DIRECTORY);
			if(TelnetUtil.validateIfPathExistForDirectory(finalPath)) {
				CURRENT_DIRECTORY = finalPath;
			}

			return finalPath;


		case MKDIR:
			String stringAfterMKDIR = null;
			try{
				stringAfterMKDIR = stk.nextToken();
			}catch(Exception ex){stringAfterMKDIR = null;}

			String outfile = ostype.executeMKDIRCommand(stringAfterMKDIR, CURRENT_DIRECTORY);
			if(outfile != null) {
			   return "";
			}else{

			}
		case CP:
			return TelnetUtil.YET_TO_IMPLEMENT;
		case MV:
		   return TelnetUtil.YET_TO_IMPLEMENT;
		default:
            return  wcmd.toString()+":" + TelnetUtil.COMMAND_NOT_FIND;
	    }	
	
    }
	
	
	/** This method is use to parse unix commands
	 * 
	 * @param wcmd
	 * @param stk
	 * @param ostype
	 * @return
	 */
	private String parseWindowsCmdSwitch(CLICommands.OSCommands wcmd, StringTokenizer stk, OSType ostype) {
		switch (wcmd) {
       
		case PWD:				
			return CURRENT_DIRECTORY;
        case DEL:
        	String delPath = TelnetUtil.getImmediateNextToken(stk);
        	return ostype.removeFileOrDirectory(delPath, false, CURRENT_DIRECTORY);
		case DIR:
			String filesList = ostype.listCurrentDirecotryDetails(null, CURRENT_DIRECTORY);
			return filesList;
		case CD:
			String stringAfterCD = null;
			try{
				stringAfterCD = stk.nextToken();
			}catch(Exception ex){stringAfterCD = null;}

			String finalPath = ostype.executeCDCommand(stringAfterCD, CURRENT_DIRECTORY);
			if(TelnetUtil.validateIfPathExistForDirectory(finalPath)) {
				CURRENT_DIRECTORY = finalPath;
			}

			return finalPath;
		case MKDIR:
			String stringAfterMKDIR = null;
			try{
				stringAfterMKDIR = stk.nextToken();
			}catch(Exception ex){stringAfterMKDIR = null;}

			String outfile = ostype.executeMKDIRCommand(stringAfterMKDIR, CURRENT_DIRECTORY);
			if(outfile != null) {
			   return "";
			}else{

			}
		default:
            return wcmd.toString()+", " +TelnetUtil.WINDOWS_CMD_NOT_FOUND;
        }
	}
	
}



