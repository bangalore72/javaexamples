package com.telnet.core.osfactory;

import java.io.File;
import java.io.IOException;

import com.telnet.core.interfaces.OSType;
import com.telnet.core.util.TelnetUtil;
import com.telnet.core.util.WindowsUtil;

public class WindowsOS implements OSType{


	public String getPresentWorkingDirectory() {
		String myCurrentDir = System.getProperty("user.dir")
				+ "\\"
				+ System.getProperty("sun.java.command")
						.substring(
								0,
								System.getProperty("sun.java.command")
										.lastIndexOf("."))
						.replace(".", "\\");
		
		return myCurrentDir;
		
	}

	//command   DIR

	public String listCurrentDirecotryDetails(String cmdParams, String path) {
		
		try {
			return WindowsUtil.listCurrentDirectoryDetails(path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Error: Failed to get the list";
		}
	}

	public String executeCDCommand(String cdInputPath, String path) {
		return TelnetUtil.executeCDCommand(cdInputPath, path);
	}

	
	public String executeMKDIRCommand(String mkdirInput, String path) {
		
		return TelnetUtil.executeMKDIRCommand(mkdirInput, path);
	}


    public String removeFileOrDirectory(String delPath, boolean forceRemove, String currentPath) {
       return WindowsUtil.deleteDirOrFile(delPath, currentPath);
    }
}
