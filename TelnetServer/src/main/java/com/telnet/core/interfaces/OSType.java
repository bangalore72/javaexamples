package com.telnet.core.interfaces;

public interface OSType {
    
	/** "PWD"
	 * 
	 * @return
	 */
	public String getPresentWorkingDirectory();
	
	/**Based on OSType command varies. on Unix ls -lrt 
	 * in Windows DIR
	 * 
	 * @param command
	 * @param path
	 * @return
	 */
	public String listCurrentDirecotryDetails(String cmdparams, String path);

    /**Cd command
     *
     * @param cdInput
     * @param path
     * @return
     */
	public String executeCDCommand(String cdInput, String path);

    /** MKDIR command
     *
      * @param mkdirInput
     * @param path
     * @return
     */
	public String executeMKDIRCommand(String mkdirInput, String path);

    public String removeFileOrDirectory(String params, boolean forceRemove, String currentPath);
}
