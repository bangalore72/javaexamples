package com.telnet.core.util;

public interface CLICommands {
	
      public enum OSCommands {
    	  PWD("pwd"),
    	  CD("cd"),
          RM("rm"),
          DEL("del"),
    	  DIR("dir"),
    	  CP("cp"),
    	  MV("mv"),
    	  MKDIR("mkdir"),
          LS("ls");
    	  private String command;
    	  
    	  OSCommands(String command)  {
    		  this.command = command;
    	  }
    	  
    	  
    	  public String getCommand() {
    		  return this.command;
    	  }
    	  
    	  public String toString() {
    		  return this.command;
    	  }
    	  
    	  
    	  public static OSCommands value(String command) {
              if(command.equals(OSCommands.PWD.toString()))
                  return OSCommands.PWD;
              else if(command.equals(OSCommands.CD.toString()))
                  return OSCommands.CD;
              else if(command.equals(OSCommands.DIR.toString()))
                  return OSCommands.DIR;
              else if(command.equals(OSCommands.MKDIR.toString()))
                  return OSCommands.MKDIR;
              else if(command.equals(OSCommands.LS.toString()))
                  return OSCommands.LS;
              else if(command.equals(OSCommands.RM.toString()))
                  return OSCommands.RM;
              else if(command.equals(OSCommands.DEL.toString())) 
            	  return OSCommands.DEL;
              else if(command.equals(OSCommands.CP.toString())) 
            	  return OSCommands.CP;
              else if(command.equals(OSCommands.MV.toString())) 
            	  return OSCommands.MV;
              else
                  return null;
          }
    	  
      }
      
      
      public enum UnixCommands {
    	  PWD("pwd"),
    	  CD("cd"),
    	  LS("ls");
    	  
    	  private String command;
    	  
    	  UnixCommands(String command)  {
    		  this.command = command;
    	  }
    	  
    	  public String getCommand() {
    		  return this.command;
    	  }
    	  
    	  public String toString() {
    		  return this.command;
    	  }
      }
}
