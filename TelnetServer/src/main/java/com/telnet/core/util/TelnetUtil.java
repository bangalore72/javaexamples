package com.telnet.core.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.PosixFileAttributes;
import java.nio.file.attribute.PosixFilePermissions;
import java.nio.file.attribute.FileTime;
import java.util.*;
import java.text.SimpleDateFormat;

import com.telnet.core.multithreadmodel.DirFileVisitor;
import com.telnet.core.interfaces.OSType;

public class TelnetUtil {

    public static final String UNABLE_TO_FIND_COMMAND_ERROR = "Unable to find the command";
    public static final String RM_ERROR_MSG_UNIX = "rm: missing operand \\n Try \\`rm --help\' for more information;";    
    public static final String COMMAND_NOT_SUPPORTED = "Command not supported.";
    public static final String COMMAND_NOT_FIND = " command not found ";
    public static final String EMPTY_STRING = "";
    public static final String SUCCESSFULLY_DELETED = "Successfully deleted. ";
    public static final String WINDOWS_CMD_NOT_FOUND = " not recognized as an internal or external command,"+ "operable program or batch file.";
    public static final String YET_TO_IMPLEMENT = " Yet to implement. Comming soon.";


    /** Method to get home direcotry for any OS
     *
     * @return
     */
	public static String getUserHomeDirectory(){
		       
         
        // We get the path by getting the system property with the 
        // defined key above.
        return System.getProperty("user.home");
		
	}    

   

    /** path validator . This method validates path for only directories. because for "cd" command files wont be considered as valid path
     *
     * @param path
     * @return
     */
	public static boolean validateIfPathExistForDirectory(String path) {
		
		File f = new File(path);
		if (!f.exists()) {
			return false;
		} else if (!f.isDirectory()) {
			return false;
		} 
		
		return true;
	}

    /**  This method just returns if the path exist. This method validates path only for files. For directories it returns as invalid path
     *
     * @param path
     * @return
     */
    public static boolean validateIfPathExistExcludeDirectory(String path) {

		File f = new File(path);
		if (!f.exists()) {
			return false;
		}else if(!f.isFile()) {
            return false;
        }

		return true;
	}

    /**
     * 
     * @param path
     * @return
     */
    public static boolean validateOnlyPath(String path) {
       File f = new File(path);
		if (!f.exists()) {
			return false;
		}

        return true;
    }

    /** This method is to replace with proper slashes based on OS. for example unix supports "/".
     *
     * @param cdPath
     * @return
     */
    public static String rephrasePathForBackslashes(String cdPath) {

        if(FindOS.isWindows()) {
            cdPath =  cdPath.replace('/', '\\');
        }else if(FindOS.isUnix()){
            cdPath =  cdPath.replace('\\', '/');
        }

        return  cdPath;
    }

    /** Execute CD Command
     *
     * @param cdDetails
     * @param CurrentDirectory
     * @return
     */
	public static String executeCDCommand(String cdDetails, String CurrentDirectory) {

		cdDetails = rephrasePathForBackslashes(cdDetails);   

        if(cdDetails != null && cdDetails.indexOf("..") >= 0) {
			CurrentDirectory = parseCDForDots(cdDetails, CurrentDirectory);
			return CurrentDirectory;
		}else if (cdDetails != null) {
			System.out.println(" nextToken " + cdDetails);
            
			String tempPath = OSBasedOPsUtil.getPathByOSType(CurrentDirectory,  cdDetails);

			if (TelnetUtil.validateIfPathExistForDirectory(tempPath)) {				
				return tempPath;
			} else {
               return   OSBasedOPsUtil.returnCDErrorMessageByOSType(tempPath);
			}
		} else {
			return CurrentDirectory;
		}
	}

    /** Execute CD with dots for ex:  cd ..
     *
     * @param cdcmd
     * @param currDirectory
     * @return
     */
    public static String parseCDForDots(String cdcmd, String currDirectory) {
		StringTokenizer stk = OSBasedOPsUtil.breakStringBasedOnOS(cdcmd);
		while(stk.hasMoreTokens())  {
			String token = stk.nextToken();
			if(token.equalsIgnoreCase("..")) {
				currDirectory = removeLastPathFromCurrentDirecotry(currDirectory);
			}else {
				currDirectory = currDirectory + OSBasedOPsUtil.getSlashBasedOnOSType() + token;
			}
		}

		return currDirectory;
	}

   

   

    /** this method is to answer .. for cd command. This will remove last directory from the path
     *
     * @param currDrpath
     * @return
     */
    public static String removeLastPathFromCurrentDirecotry(String currDrpath) {
		StringTokenizer stks = OSBasedOPsUtil.breakStringBasedOnOS(currDrpath);
		List<String> list = new ArrayList<String>();
		while(stks.hasMoreTokens()){
			list.add(stks.nextToken());
		}

		list.remove(list.get(list.size() - 1));

		String path = OSBasedOPsUtil.buildPathBasedOnOSType(list);

		System.out.println("cd ..  return path "+ path);

		return path;
	}

    /** method for "mkdir"
     *
     * @param mkdirDetails
     * @param currentDirectory
     * @return
     */
    public static synchronized String executeMKDIRCommand(String mkdirDetails,
			String currentDirectory) {
		if (mkdirDetails != null) {
			if (isMultiDirectory(mkdirDetails)) {
				File files = new File(currentDirectory + "\\"+mkdirDetails);
				if (files.exists()) {
					if (files.mkdirs()) {
						return "Multiple directories are created!";
					} else {
						return "Failed to create multiple directories!";
					}
				}
			} else {
				File file = new File(currentDirectory + OSBasedOPsUtil.getSlashBasedOnOSType()+mkdirDetails);
				if (!file.exists()) {
					if (file.mkdir()) {
						return "Directory is created!";
					} else {
						return "Failed to create directory!";
					}
				}
			}
		} else {
			return "The syntax of the command is incorrect.";
		}
		
		return null;

	}

    /**  validator for multidirectory
     *
     * @param multipath
     * @return
     */
	public static boolean isMultiDirectory(String multipath) {
		StringTokenizer stk = new StringTokenizer(multipath, "\\");
		if(stk.countTokens() > 1) return true;
		else return false;
	}

    /** This method is used to delete only files. 
     *
     * @param files
     * @return
     */
    public static List<String> deleteFilesOnly(Path files) {

        final List<String> errList = new ArrayList<String>();

        try {
            Files.walkFileTree(files, new SimpleFileVisitor<Path>() {

                @Override
                public FileVisitResult visitFile(Path file,
                                                 BasicFileAttributes attrs) throws IOException {

                    System.out.println("Deleting file: " + file);
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir,
                                                          IOException exc) throws IOException {

                    System.out.println("Deleting failed because it is a dir: " + dir);
                    if (exc == null) {
                        errList.add("rm: cannot remove "+  dir.getFileName() +": Is a directory");
                        return FileVisitResult.CONTINUE;
                    } else {
                        throw exc;
                    }
                }

            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return errList;

    }

    /** This method is to remove files and direcotry from the path iterating over the file directory
     *
     * @param dir
     */
    public static void deletingDirecotryRecursively(Path dir) {
        try {
            Files.walkFileTree(dir, new SimpleFileVisitor<Path>() {

                @Override
                public FileVisitResult visitFile(Path file,
                                                 BasicFileAttributes attrs) throws IOException {

                    System.out.println("Deleting file: " + file);
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir,
                                                          IOException exc) throws IOException {

                    System.out.println("Deleting dir: " + dir);
                    if (exc == null) {
                        Files.delete(dir);
                        return FileVisitResult.CONTINUE;
                    } else {
                        throw exc;
                    }
                }

            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** To parse string as tokens
     *
     * @param stk
     * @return
     */
    public static List<String> parseStringTokenFurther(StringTokenizer stk) {
        List<String> strAfterRMCmd = null;

        if(stk != null) {
            strAfterRMCmd = new ArrayList<String>();
            while(stk.hasMoreElements()) {
                strAfterRMCmd.add(stk.nextToken());
            }
        }

        return strAfterRMCmd;
    }
    
    /**
     * 
     * @param stks
     * @return
     */
    public static String getImmediateNextToken(StringTokenizer stks) {
    	if(stks != null) {
    	   return stks.nextToken();
    	}
    	
    	return EMPTY_STRING;
    }


    public static String getRMErrorBasedOnOS(String errMsg){
        if(errMsg != null) {
            return errMsg;
        }
        if(FindOS.isWindows()) {
           
        }else if(FindOS.isUnix()) {
            return RM_ERROR_MSG_UNIX;
        }

        return "";
    }
}
