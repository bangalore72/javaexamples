package com.telnet.core.util;

/**
 * Created by IntelliJ IDEA.
 * User: nareshk
 * Date: Dec 28, 2012
 * Time: 10:08:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class FileAttributes {

     private String permissions;
     private String noOfLinks;
     private String owner;
     private String group;
     private String size;
     private String lastModified;
     private String name;

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNoOfLinks() {
        return noOfLinks;
    }

    public void setNoOfLinks(String noOfLinks) {
        this.noOfLinks = noOfLinks;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }

    public String getSize() {
        return size;
    }

    public String formate(){
        return permissions + "   " + noOfLinks + "   "+ owner+ "   "+group+"   "+ size + "   "+lastModified+"   "+ name;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
