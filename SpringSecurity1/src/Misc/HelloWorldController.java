package Misc;




import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

@Controller
public class HelloWorldController{	
	
	
	@RequestMapping(value="/moveToEmailPage", method = RequestMethod.GET)
	public String moveToEmailPage(@RequestParam("emailid") String email, Model model) {
		
		model.addAttribute("msg", email);
		return "HelloWorldThirdPage";
	}
	

}
