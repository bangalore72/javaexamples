package shutdownHook;

public class ShutdownHookDemo {
	
	public void begin() {
	     System.out.println("Im inside begin");
	     ShutdownHook shutdownHook = new ShutdownHook();
	     Runtime.getRuntime().addShutdownHook(shutdownHook);  //Registering in the current runtime.
	  }

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ShutdownHookDemo shutdownHookDemo = new ShutdownHookDemo();
		  shutdownHookDemo.begin();
		  try {
		   System.in.read();
		  }catch(Exception e) { }
   }
}
